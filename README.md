# Laminas Module lerp/location

[Lingana ERP](https://linganaerp.de)

## default location
The first location* in database - with ID 1 - is always the default location. You can rename it.

It means: default is: case-1 in row-1 in rack-1 in room-1 in place-1
