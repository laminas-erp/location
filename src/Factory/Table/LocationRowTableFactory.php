<?php

namespace Lerp\Location\Factory\Table;

use Interop\Container\ContainerInterface;
use Lerp\Location\Table\LocationRowTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class LocationRowTableFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $table = new LocationRowTable();
        $table->setLogger($container->get('logger'));
        $table->setDbAdapter($container->get('dbDefault'));
        return $table;
    }
}
