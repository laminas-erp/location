<?php

namespace Lerp\Location\Factory\Service;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Lerp\Location\Service\LocationService;
use Lerp\Location\Table\LocationCaseTable;
use Lerp\Location\Table\LocationPlaceTable;
use Lerp\Location\Table\LocationRackTable;
use Lerp\Location\Table\LocationRoomTable;
use Lerp\Location\Table\LocationRowTable;
use Lerp\Location\Table\LocationViewTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class LocationServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new LocationService();
        $service->setLabelPres($container->get('config')['lerp_location']['label_pres']);
        $service->setLogger($container->get('logger'));
        $service->setLocationViewTable($container->get(LocationViewTable::class));
        $service->setLocationPlaceTable($container->get(LocationPlaceTable::class));
        $service->setLocationRoomTable($container->get(LocationRoomTable::class));
        $service->setLocationRackTable($container->get(LocationRackTable::class));
        $service->setLocationRowTable($container->get(LocationRowTable::class));
        $service->setLocationCaseTable($container->get(LocationCaseTable::class));
        return $service;
    }
}
