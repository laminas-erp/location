<?php

namespace Lerp\Location\Factory\Controller\Rest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Location\Controller\Rest\LocationRestController;
use Lerp\Location\Service\LocationService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class LocationRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new LocationRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setLocationService($container->get(LocationService::class));
        return $controller;
    }
}
