<?php

namespace Lerp\Location\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class LocationRowTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'location_row';

    /**
     * @param string $locationRowUuid
     * @return array
     */
    public function getLocationRow(string $locationRowUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['location_row_uuid' => $locationRowUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $locationRackUuid
     * @return array
     */
    public function getLocationRowsForRack(string $locationRackUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['location_rack_uuid' => $locationRackUuid]);
            $select->order('location_row_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getLocationRowIdMax(string $locationRackUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['id_max' => new Expression('MAX(location_row_id)')]);
            $select->where(['location_rack_uuid' => $locationRackUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return intval($result->toArray()[0]['id_max']);
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

    public function getLocationRowWithId(int $locationRowId, string $locationRackUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'location_row_id'    => $locationRowId,
                'location_rack_uuid' => $locationRackUuid,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $locationRackUuid
     * @return array The [UUID => label, ...] array.
     */
    public function getLocationRowsForRackUuidAssoc(string $locationRackUuid): array
    {
        $arr = $this->getLocationRowsForRack($locationRackUuid);
        if (empty($arr)) {
            return [];
        }
        $uuidAssoc = [];
        foreach ($arr as $row) {
            $uuidAssoc[$row['location_row_uuid']] = $row['location_row_label'];
        }
        return $uuidAssoc;
    }

    /**
     * @param string $locationRowUuid
     * @return bool
     */
    public function existLocationRow(string $locationRowUuid): bool
    {
        return !empty($this->getLocationRow($locationRowUuid));
    }

    /**
     * @param string $locationRackUuid
     * @param string $locationRowLabel
     * @param string $locationRowName
     * @return string
     */
    public function insertLocationRow(string $locationRackUuid, string $locationRowLabel, string $locationRowName): string
    {
        $maxId = $this->getLocationRowIdMax($locationRackUuid);
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_row_uuid'  => $uuid,
                'location_row_id'    => ++$maxId,
                'location_rack_uuid' => $locationRackUuid,
                'location_row_label' => $locationRowLabel,
                'location_row_name'  => $locationRowName,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getLocationRowDefault(string $locationRackUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'location_row_id'    => 1,
                'location_rack_uuid' => $locationRackUuid
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertLocationRowDefault(string $labelPreRow, string $locationRackUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_row_uuid'  => $uuid,
                'location_row_id'    => 1,
                'location_rack_uuid' => $locationRackUuid,
                'location_row_label' => $labelPreRow . 1,
                'location_row_name'  => 'default'
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertLocationRowWithId(int $locationRowId, string $locationRackUuid, string $locationRowLabel, string $locationRowName): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_row_uuid'  => $uuid,
                'location_row_id'    => $locationRowId,
                'location_rack_uuid' => $locationRackUuid,
                'location_row_label' => $locationRowLabel,
                'location_row_name'  => $locationRowName,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}
