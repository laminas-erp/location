<?php

namespace Lerp\Location\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Expression;

class LocationPlaceTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'location_place';

    /**
     * @param string $locationPlaceUuid
     * @return array
     */
    public function getLocationPlace(string $locationPlaceUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['location_place_uuid' => $locationPlaceUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getLocationPlaceDefault(): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'location_place_id' => 1
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $locationPlaceUuid
     * @return bool
     */
    public function existLocationPlace(string $locationPlaceUuid): bool
    {
        return !empty($this->getLocationPlace($locationPlaceUuid));
    }

    public function getLocationPlaceWithId(string $locationPlaceId): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'location_place_id' => $locationPlaceId
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $locationPlaceName
     * @param string $locationPlaceLabel
     * @return string
     */
    public function insertLocationPlace(string $locationPlaceName, string $locationPlaceLabel): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_place_uuid' => $uuid,
                'location_place_name' => $locationPlaceName,
                'location_place_label' => $locationPlaceLabel
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertLocationPlaceDefault(string $labelPrePlace): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_place_uuid' => $uuid,
                'location_place_id' => 1,
                'location_place_name' => 'default',
                'location_place_label' => $labelPrePlace . 1
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertLocationPlaceWithId(int $locationPlaceId, string $locationPlaceName, string $locationPlaceLabel): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_place_uuid' => $uuid,
                'location_place_id' => $locationPlaceId,
                'location_place_name' => $locationPlaceName,
                'location_place_label' => $locationPlaceLabel
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @return array
     */
    public function getLocationPlaces(): array
    {
        $select = $this->sql->select();
        try {
            $select->order('location_place_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @return array The [ID => label, ...] array.
     */
    public function getLocationPlacesUuidAssoc(): array
    {
        $locationPlaces = $this->getLocationPlaces();
        if (empty($locationPlaces)) {
            return [];
        }
        $idAssoc = [];
        foreach ($locationPlaces as $locationPlace) {
            $idAssoc[$locationPlace['location_place_uuid']] = $locationPlace['location_place_label'];
        }
        return $idAssoc;
    }

    public function getLocationPlaceIdMax(): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['id_max' => new Expression('MAX(location_place_id)')]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return intval($result->toArray()[0]['id_max']);
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return 0;
    }
}
