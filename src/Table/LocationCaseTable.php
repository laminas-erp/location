<?php

namespace Lerp\Location\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class LocationCaseTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'location_case';

    /**
     * @param string $locationCaseUuid
     * @return array
     */
    public function getLocationCase(string $locationCaseUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['location_case_uuid' => $locationCaseUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existLocationCase(string $locationCaseUuid): bool
    {
        return !empty($this->getLocationCase($locationCaseUuid));
    }

    /**
     * @param string $locationRowUuid
     * @return array
     */
    public function getLocationCasesForRow(string $locationRowUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['location_row_uuid' => $locationRowUuid]);
            $select->order('location_case_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getLocationCaseWithId(int $locationCaseId, string $locationRowUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'location_case_id'  => $locationCaseId,
                'location_row_uuid' => $locationRowUuid,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getLocationCaseIdMax(string $locationRowUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['id_max' => new Expression('MAX(location_case_id)')]);
            $select->where(['location_row_uuid' => $locationRowUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return intval($result->toArray()[0]['id_max']);
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

    /**
     * @param string $locationRowUuid
     * @return array The [ID => label, ...] array.
     */
    public function getLocationCasesForRowUuidAssoc(string $locationRowUuid): array
    {
        $arr = $this->getLocationCasesForRow($locationRowUuid);
        if (empty($arr)) {
            return [];
        }
        $idAssoc = [];
        foreach ($arr as $row) {
            $idAssoc[$row['location_case_uuid']] = $row['location_case_label'];
        }
        return $idAssoc;
    }

    /**
     * @param int $locationRowId
     * @param string $locationCaseLabel
     * @param string $locationCaseName
     * @return string
     */
    public function insertLocationCase(int $locationRowId, string $locationCaseLabel, string $locationCaseName): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_case_uuid'  => $uuid,
                'location_row_id'     => $locationRowId,
                'location_case_label' => $locationCaseLabel,
                'location_case_name'  => $locationCaseName,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getLocationCaseDefault(string $locationRowUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'location_case_id'  => 1,
                'location_row_uuid' => $locationRowUuid
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertLocationCaseDefault(string $labelPreCase, string $locationRowUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_case_uuid'  => $uuid,
                'location_case_id'    => 1,
                'location_row_uuid'   => $locationRowUuid,
                'location_case_label' => $labelPreCase . 1,
                'location_case_name'  => 'default'
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertLocationCaseWithId(int $locationCaseId, string $locationRowUuid, string $locationCaseLabel, string $locationCaseName): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_case_uuid'  => $uuid,
                'location_case_id'    => $locationCaseId,
                'location_row_uuid'   => $locationRowUuid,
                'location_case_label' => $locationCaseLabel,
                'location_case_name'  => $locationCaseName,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateLocationCaseName(string $caseUuid, string $caseName, string $caseLabel): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['location_case_name' => $caseName, 'location_case_label' => $caseLabel]);
            $update->where(['location_case_uuid' => $caseUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
