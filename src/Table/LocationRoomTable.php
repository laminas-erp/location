<?php

namespace Lerp\Location\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class LocationRoomTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'location_room';

    /**
     * @param string $locationRoomUuid
     * @return array
     */
    public function getLocationRoom(string $locationRoomUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['location_room_uuid' => $locationRoomUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getLocationRoomsForPlace(string $locationPlaceUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['location_place_uuid' => $locationPlaceUuid]);
            $select->order('location_room_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getLocationRoomWithId(int $locationRoomId, string $locationPlaceUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'location_room_id' => $locationRoomId,
                'location_place_uuid' => $locationPlaceUuid,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getLocationRoomIdMax(string $locationPlaceUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['id_max' => new Expression('MAX(location_room_id)')]);
            $select->where(['location_place_uuid' => $locationPlaceUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return intval($result->toArray()[0]['id_max']);
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

    /**
     * @param string $locationPlaceUuid
     * @return array The [UUID => label, ...] array.
     */
    public function getLocationRoomsForPlaceUuidAssoc(string $locationPlaceUuid): array
    {
        $arr = $this->getLocationRoomsForPlace($locationPlaceUuid);
        if (empty($arr)) {
            return [];
        }
        $idAssoc = [];
        foreach ($arr as $row) {
            $idAssoc[$row['location_room_uuid']] = $row['location_room_label'];
        }
        return $idAssoc;
    }

    /**
     * @param string $locationRoomUuid
     * @return bool
     */
    public function existLocationRoom(string $locationRoomUuid): bool
    {
        return !empty($this->getLocationRoom($locationRoomUuid));
    }

    /**
     * @param string $locationPlaceUuid
     * @param string $locationRoomName
     * @param string $locationRoomLabel
     * @return int
     */
    public function insertLocationRoom(string $locationPlaceUuid, string $locationRoomName, string $locationRoomLabel): int
    {
        $maxId = $this->getLocationRoomIdMax($locationPlaceUuid);
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_room_uuid' => $uuid,
                'location_room_id' => ++$maxId,
                'location_place_uuid' => $locationPlaceUuid,
                'location_room_name' => $locationRoomName,
                'location_room_label' => $locationRoomLabel
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getLocationRoomDefault(string $locationPlaceUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'location_room_id' => 1,
                'location_place_uuid' => $locationPlaceUuid
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertLocationRoomDefault(string $labelPreRoom, string $locationPlaceUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_room_uuid' => $uuid,
                'location_room_id' => 1,
                'location_place_uuid' => $locationPlaceUuid,
                'location_room_name' => 'default',
                'location_room_label' => $labelPreRoom . 1
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertLocationRoomWithId(int $locationRoomId, string $locationPlaceUuid, string $locationRoomName, string $locationRoomLabel): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_room_uuid' => $uuid,
                'location_room_id' => $locationRoomId,
                'location_place_uuid' => $locationPlaceUuid,
                'location_room_name' => $locationRoomName,
                'location_room_label' => $locationRoomLabel
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}
