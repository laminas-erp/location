<?php

namespace Lerp\Location\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class LocationViewTable extends AbstractLibTable
{
    /**
     * @var string
     */
    protected $table = 'view_location_case';

    /**
     * @return array Location with all location_*_id == 1
     */
    public function getLocationDefault(): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'location_place_id' => 1,
                'location_room_id'  => 1,
                'location_rack_id'  => 1,
                'location_row_id'   => 1,
                'location_case_id'  => 1,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $locationPlaceUuid
     * @param string $locationRoomUuid
     * @param string $locationRackUuid
     * @param string $locationRowUuid
     * @return array
     */
    public function getLocationCases(string $locationPlaceUuid, string $locationRoomUuid, string $locationRackUuid, string $locationRowUuid): array
    {
        $select = $this->sql->select();
        try {
            if (!empty($locationPlaceUuid)) {
                $select->where(['location_place_uuid' => $locationPlaceUuid]);
            }
            if (!empty($locationRoomUuid)) {
                $select->where(['location_room_uuid' => $locationRoomUuid]);
            }
            if (!empty($locationRackUuid)) {
                $select->where(['location_rack_uuid' => $locationRackUuid]);
            }
            if (!empty($locationRowUuid)) {
                $select->where(['location_row_uuid' => $locationRowUuid]);
            }
            $select->order('location_place_id ASC, location_room_id ASC, location_rack_id ASC, location_row_id ASC, location_case_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $locationPlaceUuid
     * @param string $locationRoomUuid
     * @param string $locationRackUuid
     * @return array
     */
    public function getLocationRows(string $locationPlaceUuid, string $locationRoomUuid, string $locationRackUuid): array
    {
        $select = new Select('view_location_row');
        try {
            if (!empty($locationPlaceUuid)) {
                $select->where(['location_place_uuid' => $locationPlaceUuid]);
            }
            if (!empty($locationRoomUuid)) {
                $select->where(['location_room_uuid' => $locationRoomUuid]);
            }
            if (!empty($locationRackUuid)) {
                $select->where(['location_rack_uuid' => $locationRackUuid]);
            }
            $select->order('location_place_id ASC, location_room_id ASC, location_rack_id ASC, location_row_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getLocationRacks(string $locationPlaceUuid, string $locationRoomUuid): array
    {
        $select = new Select('view_location_rack');
        try {
            if (!empty($locationPlaceUuid)) {
                $select->where(['location_place_uuid' => $locationPlaceUuid]);
            }
            if (!empty($locationRoomUuid)) {
                $select->where(['location_room_uuid' => $locationRoomUuid]);
            }
            $select->order('location_place_id ASC, location_room_id ASC, location_rack_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $locationPlaceUuid
     * @return array
     */
    public function getLocationRooms(string $locationPlaceUuid): array
    {
        $select = new Select('view_location_room');
        try {
            if (!empty($locationPlaceUuid)) {
                $select->where(['location_place_uuid' => $locationPlaceUuid]);
            }
            $select->order('location_place_id ASC, location_room_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param int $caseId
     * @param int $rowId
     * @param int $rackId
     * @param int $roomId
     * @param int $placeId
     * @return array
     */
    public function getLocationWithIds(int $caseId, int $rowId, int $rackId, int $roomId, int $placeId): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'location_case_id'  => $caseId,
                'location_row_id'   => $rowId,
                'location_rack_id'  => $rackId,
                'location_room_id'  => $roomId,
                'location_place_id' => $placeId,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
