<?php

namespace Lerp\Location\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class LocationRackTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'location_rack';

    /**
     * @param string $locationRackUuid
     * @return array
     */
    public function getLocationRack(string $locationRackUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['location_rack_uuid' => $locationRackUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getLocationRacksForRoom(string $locationRoomUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['location_room_uuid' => $locationRoomUuid]);
            $select->order('location_rack_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getLocationRackIdMax(string $locationRoomUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['id_max' => new Expression('MAX(location_rack_id)')]);
            $select->where(['location_room_uuid' => $locationRoomUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return intval($result->toArray()[0]['id_max']);
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return 0;
    }

    public function getLocationRackWithId(int $locationRackId, string $locationRoomUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'location_rack_id'   => $locationRackId,
                'location_room_uuid' => $locationRoomUuid,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $locationRoomUuid
     * @return array The [UUID => label, ...] array.
     */
    public function getLocationRacksForRoomUuidAssoc(string $locationRoomUuid): array
    {
        $arr = $this->getLocationRacksForRoom($locationRoomUuid);
        if (empty($arr)) {
            return [];
        }
        $idAssoc = [];
        foreach ($arr as $row) {
            $idAssoc[$row['location_rack_uuid']] = $row['location_rack_label'];
        }
        return $idAssoc;
    }

    /**
     * @param string $locationRackUuid
     * @return bool
     */
    public function existLocationRack(string $locationRackUuid): bool
    {
        return !empty($this->getLocationRack($locationRackUuid));
    }

    /**
     * @param string $locationRoomUuid
     * @param string $locationRackLabel
     * @param string $locationRackName
     * @return string location_rack_uuid
     */
    public function insertLocationRack(string $locationRoomUuid, string $locationRackLabel, string $locationRackName): string
    {
        $maxId = $this->getLocationRackIdMax($locationRoomUuid);
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_rack_uuid'  => $uuid,
                'location_rack_id'    => ++$maxId,
                'location_room_uuid'  => $locationRoomUuid,
                'location_rack_label' => $locationRackLabel,
                'location_rack_name'  => $locationRackName,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getLocationRackDefault(string $locationRoomUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'location_rack_id'   => 1,
                'location_room_uuid' => $locationRoomUuid
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertLocationRackDefault(string $labelPreRack, string $locationRoomUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_rack_uuid'  => $uuid,
                'location_rack_id'    => 1,
                'location_room_uuid'  => $locationRoomUuid,
                'location_rack_label' => $labelPreRack . 1,
                'location_rack_name'  => 'default'
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertLocationRackWithId(int $locationRackId, string $locationRoomUuid, string $locationRackLabel, string $locationRackName): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'location_rack_uuid'  => $uuid,
                'location_rack_id'    => $locationRackId,
                'location_room_uuid'  => $locationRoomUuid,
                'location_rack_label' => $locationRackLabel,
                'location_rack_name'  => $locationRackName,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}
