<?php

namespace Lerp\Location\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Location\Entity\LocationEntity;
use Lerp\Location\Table\LocationCaseTable;
use Lerp\Location\Table\LocationPlaceTable;
use Lerp\Location\Table\LocationRackTable;
use Lerp\Location\Table\LocationRoomTable;
use Lerp\Location\Table\LocationRowTable;
use Lerp\Location\Table\LocationViewTable;

class LocationService extends AbstractService
{
    protected string $labelPrePlace;
    protected string $labelPreRoom;
    protected string $labelPreRack;
    protected string $labelPreRow;
    protected string $labelPreCase;
    protected LocationViewTable $locationViewTable;
    protected LocationPlaceTable $locationPlaceTable;
    protected LocationRoomTable $locationRoomTable;
    protected LocationRackTable $locationRackTable;
    protected LocationRowTable $locationRowTable;
    protected LocationCaseTable $locationCaseTable;

    public function setLabelPres(array $labelPres): void
    {
        $this->labelPrePlace = $labelPres['place'];
        $this->labelPreRoom = $labelPres['room'];
        $this->labelPreRack = $labelPres['rack'];
        $this->labelPreRow = $labelPres['row'];
        $this->labelPreCase = $labelPres['case'];
    }

    public function setLocationViewTable(LocationViewTable $locationViewTable): void
    {
        $this->locationViewTable = $locationViewTable;
    }

    public function setLocationPlaceTable(LocationPlaceTable $locationPlaceTable): void
    {
        $this->locationPlaceTable = $locationPlaceTable;
    }

    public function setLocationRoomTable(LocationRoomTable $locationRoomTable): void
    {
        $this->locationRoomTable = $locationRoomTable;
    }

    public function setLocationRackTable(LocationRackTable $locationRackTable): void
    {
        $this->locationRackTable = $locationRackTable;
    }

    public function setLocationRowTable(LocationRowTable $locationRowTable): void
    {
        $this->locationRowTable = $locationRowTable;
    }

    public function setLocationCaseTable(LocationCaseTable $locationCaseTable): void
    {
        $this->locationCaseTable = $locationCaseTable;
    }

    /**
     * Insert default values with primary key = 1.
     */
    public function checkInsertDefaultLocations(): void
    {
        if (empty($placeDefault = $this->locationPlaceTable->getLocationPlaceDefault())) {
            $placeUuid = $this->locationPlaceTable->insertLocationPlaceDefault($this->labelPrePlace);
        } else {
            $placeUuid = $placeDefault['location_place_uuid'];
        }
        if (empty($roomDefault = $this->locationRoomTable->getLocationRoomDefault($placeUuid))) {
            $roomUuid = $this->locationRoomTable->insertLocationRoomDefault($this->labelPreRoom, $placeUuid);
        } else {
            $roomUuid = $roomDefault['location_room_uuid'];
        }
        if (empty($rackDefault = $this->locationRackTable->getLocationRackDefault($roomUuid))) {
            $rackUuid = $this->locationRackTable->insertLocationRackDefault($this->labelPreRack, $roomUuid);
        } else {
            $rackUuid = $rackDefault['location_rack_uuid'];
        }
        if (empty($rowDefault = $this->locationRowTable->getLocationRowDefault($rackUuid))) {
            $rowUuid = $this->locationRowTable->insertLocationRowDefault($this->labelPreRow, $rackUuid);
        } else {
            $rowUuid = $rowDefault['location_row_uuid'];
        }
        if (empty($this->locationCaseTable->getLocationCaseDefault($rowUuid))) {
            $this->locationCaseTable->insertLocationCaseDefault($this->labelPreCase, $rowUuid);
        }
    }

    /**
     * Create a new location_case in default location_row_uuid with custom name and label.
     * @param string $locationName
     * @param string $locationLabel A short name for quick reading. If it is empty, then a label from param $locationName will be computed.
     * @return string The location_case_uuid on success, else an empty string.
     */
    public function insertAddDefaultLocationCase(string $locationName, string $locationLabel = ''): string
    {
        $ld = $this->locationViewTable->getLocationDefault();
        if (
            empty($ld)
            || empty($lcIdMax = $this->locationCaseTable->getLocationCaseIdMax($ld['location_row_uuid']))
            || (empty($locationName) && empty($locationLabel))
        ) {
            return '';
        }
        if (empty($locationLabel)) {
            $locationLabel = $this->computeLocationLabelFromName($locationName);
        }
        return $this->locationCaseTable->insertLocationCaseWithId(++$lcIdMax, $ld['location_row_uuid'], $locationLabel, $locationName);
    }

    protected function computeLocationLabelFromName(string $locationName): string
    {
        return strtoupper(trim(str_replace(' ', '_', $locationName)));
    }

    /**
     * @return LocationEntity
     */
    public function getLocationDefault(): LocationEntity
    {
        $location = new LocationEntity();
        if (!$location->exchangeArrayFromDatabase($this->locationViewTable->getLocationDefault())) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Failed to create a LocationEntity - on line ' . __LINE__);
        }
        return $location;
    }

    /**
     * @param string $locationPlaceName
     * @return string
     */
    public function insertLocationPlace(string $locationPlaceName): string
    {
        $idMax = $this->locationPlaceTable->getLocationPlaceIdMax();
        $locationPlaceLabel = $this->labelPrePlace . ++$idMax;
        $locationPlaceName = $locationPlaceName ?: $locationPlaceLabel;
        return $this->locationPlaceTable->insertLocationPlaceWithId($idMax, $locationPlaceName, $locationPlaceLabel);
    }

    /**
     * @param string $locationPlaceUuid
     * @param string $locationRoomName
     * @return string
     */
    public function insertLocationRoom(string $locationPlaceUuid, string $locationRoomName): string
    {
        $idMax = $this->locationRoomTable->getLocationRoomIdMax($locationPlaceUuid);
        $locationRoomLabel = $this->labelPreRoom . ++$idMax;
        $locationRoomName = $locationRoomName ?: $locationRoomLabel;
        return $this->locationRoomTable->insertLocationRoomWithId($idMax, $locationPlaceUuid, $locationRoomName, $locationRoomLabel);
    }

    /**
     * Create a rack with a case. Without at least one case, the rack cannot be displayed.
     * @param string $locationRoomUuid
     * @param string $locationRackName
     * @return string
     */
    public function insertLocationRack(string $locationRoomUuid, string $locationRackName = ''): string
    {
        $idMax = $this->locationRackTable->getLocationRackIdMax($locationRoomUuid);
        $locationRackLabel = $this->labelPreRack . ++$idMax;
        $locationRackName = $locationRackName ?: $locationRackLabel;
        $conn = $this->beginTransaction($this->locationRowTable);
        if (
            empty($rackUuid = $this->locationRackTable->insertLocationRackWithId($idMax, $locationRoomUuid, $locationRackLabel, $locationRackName))
            || empty($this->insertLocationRowInitial($rackUuid))
        ) {
            $conn->rollback();
            return '';
        }
        $conn->commit();
        return $rackUuid;
    }

    /**
     * Create a row with one case.
     * @param string $locationRackUuid
     * @param string $locationRowName
     * @return string
     */
    protected function insertLocationRowInitial(string $locationRackUuid, string $locationRowName = ''): string
    {
        $locationRowLabel = $this->labelPreRow . 1;
        $locationRowName = $locationRowName ?: $locationRowLabel;
        if (
            empty($rowUuid = $this->locationRowTable->insertLocationRow($locationRackUuid, $locationRowName, $locationRowLabel))
            || !$this->insertLocationCase($rowUuid)
        ) {
            return '';
        }
        return $rowUuid;
    }

    /**
     * Create a row with one initial case.
     * @param string $locationRackUuid
     * @param string $locationRowName
     * @return string
     */
    public function insertLocationRow(string $locationRackUuid, string $locationRowName = ''): string
    {
        $idMax = $this->locationRowTable->getLocationRowIdMax($locationRackUuid);
        if (empty($idMax)) {
            // initial row is missing
            return '';
        }
        $locationRowLabel = $this->labelPreRow . ++$idMax;
        $locationRowName = $locationRowName ?: $locationRowLabel;
        $conn = $this->beginTransaction($this->locationRowTable);
        if (
            empty($rowUuid = $this->locationRowTable->insertLocationRowWithId($idMax, $locationRackUuid, $locationRowName, $locationRowLabel))
            || !$this->insertLocationCase($rowUuid)
        ) {
            $conn->rollback();
            return '';
        }
        $conn->commit();
        return $rowUuid;
    }

    /**
     * @param string $locationRowUuid
     * @param string $locationCaseName
     * @return bool
     */
    public function insertLocationCase(string $locationRowUuid, string $locationCaseName = ''): bool
    {
        $idMax = $this->locationCaseTable->getLocationCaseIdMax($locationRowUuid);
        $locationCaseLabel = $this->labelPreCase . ++$idMax;
        $locationCaseName = $locationCaseName ?: $locationCaseLabel;
        return !empty($this->locationCaseTable->insertLocationCaseWithId($idMax, $locationRowUuid, $locationCaseLabel, $locationCaseName));
    }

    /**
     * @param int $caseId
     * @param int $rowId
     * @param int $rackId
     * @param int $roomId
     * @param int $placeId
     * @return string location_case_uuid if the location exist or create successful, empty string on failure.
     */
    public function checkInsertLocationWithIds(int $caseId, int $rowId, int $rackId, int $roomId, int $placeId): string
    {
        if (empty($this->locationViewTable->getLocationDefault())) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Initial locations are missing.');
        }
        if (!empty($l = $this->locationViewTable->getLocationWithIds($caseId, $rowId, $rackId, $roomId, $placeId))) {
            return $l['location_case_uuid'];
        }
        return $this->insertLocationWithIds($caseId, $rowId, $rackId, $roomId, $placeId);
    }

    public function insertLocationWithIds(int $caseId, int $rowId, int $rackId, int $roomId, int $placeId): string
    {
        $conn = $this->beginTransaction($this->locationPlaceTable);
        if (empty($lPlace = $this->locationPlaceTable->getLocationPlaceWithId($placeId))) {
            $name = $this->labelPrePlace . $placeId;
            if (empty($placeUuid = $this->locationPlaceTable->insertLocationPlaceWithId($placeId, $name, $name))) {
                $conn->rollback();
                return '';
            }
        } else {
            $placeUuid = $lPlace['location_place_uuid'];
        }
        if (empty($lRoom = $this->locationRoomTable->getLocationRoomWithId($roomId, $placeUuid))) {
            $name = $this->labelPreRoom . $roomId;
            if (empty($roomUuid = $this->locationRoomTable->insertLocationRoomWithId($roomId, $placeUuid, $name, $name))) {
                $conn->rollback();
                return '';
            }
        } else {
            $roomUuid = $lRoom['location_room_uuid'];
        }
        if (empty($lRack = $this->locationRackTable->getLocationRackWithId($rackId, $roomUuid))) {
            $name = $this->labelPreRack . $rackId;
            if (empty($rackUuid = $this->locationRackTable->insertLocationRackWithId($rackId, $roomUuid, $name, $name))) {
                $conn->rollback();
                return '';
            }
        } else {
            $rackUuid = $lRack['location_rack_uuid'];
        }
        if (empty($lRow = $this->locationRowTable->getLocationRowWithId($rowId, $rackUuid))) {
            $name = $this->labelPreRow . $rowId;
            if (empty($rowUuid = $this->locationRowTable->insertLocationRowWithId($rowId, $rackUuid, $name, $name))) {
                $conn->rollback();
                return '';
            }
        } else {
            $rowUuid = $lRow['location_row_uuid'];
        }
        if (empty($lCase = $this->locationCaseTable->getLocationCaseWithId($caseId, $rowUuid))) {
            $name = $this->labelPreCase . $caseId;
            if (empty($caseUuid = $this->locationCaseTable->insertLocationCaseWithId($caseId, $rowUuid, $name, $name))) {
                $conn->rollback();
                return '';
            }
        } else {
            $caseUuid = $lCase['location_case_uuid'];
        }
        $conn->commit();
        return $caseUuid;
    }

    /**
     * @param string $caseUuid
     * @param string $caseName
     * @param string $caseLabel
     * @return bool
     */
    public function updateLocationCaseName(string $caseUuid, string $caseName, string $caseLabel): bool
    {
        return $this->locationCaseTable->updateLocationCaseName($caseUuid, $caseName, $caseLabel) >= 0;
    }

    /**
     * @param string $locationPlaceUuid
     * @return bool
     */
    public function existLocationPlace(string $locationPlaceUuid): bool
    {
        return $this->locationPlaceTable->existLocationPlace($locationPlaceUuid);
    }

    /**
     * @param string $locationRoomUuid
     * @return bool
     */
    public function existLocationRoom(string $locationRoomUuid): bool
    {
        return $this->locationRoomTable->existLocationRoom($locationRoomUuid);
    }

    /**
     * @param string $locationRackUuid
     * @return bool
     */
    public function existLocationRack(string $locationRackUuid): bool
    {
        return $this->locationRackTable->existLocationRack($locationRackUuid);
    }

    /**
     * @param string $locationRowUuid
     * @return bool
     */
    public function existLocationRow(string $locationRowUuid): bool
    {
        return $this->locationRowTable->existLocationRow($locationRowUuid);
    }

    /**
     * @param string $locationCaseUuid
     * @return bool
     */
    public function existLocationCase(string $locationCaseUuid): bool
    {
        return $this->locationCaseTable->existLocationCase($locationCaseUuid);
    }

    /**
     * @param string $locationRowUuid
     * @return array
     */
    public function getLocationCases(string $locationRowUuid): array
    {
        return $this->locationCaseTable->getLocationCasesForRow($locationRowUuid);
    }

    /**
     * @param string $locationPlaceUuid
     * @param string $locationRoomUuid
     * @param string $locationRackUuid
     * @param string $locationRowUuid
     * @return array
     */
    public function getLocationViewCases(string $locationPlaceUuid, string $locationRoomUuid, string $locationRackUuid, string $locationRowUuid): array
    {
        return $this->locationViewTable->getLocationCases($locationPlaceUuid, $locationRoomUuid, $locationRackUuid, $locationRowUuid);
    }

    /**
     * @param string $locationRackUuid
     * @return array
     */
    public function getLocationRows(string $locationRackUuid): array
    {
        return $this->locationRowTable->getLocationRowsForRack($locationRackUuid);
    }

    /**
     * @param string $locationPlaceUuid
     * @param string $locationRoomUuid
     * @param string $locationRackUuid
     * @return array
     */
    public function getLocationViewRows(string $locationPlaceUuid, string $locationRoomUuid, string $locationRackUuid): array
    {
        return $this->locationViewTable->getLocationRows($locationPlaceUuid, $locationRoomUuid, $locationRackUuid);
    }

    /**
     * @param string $locationPlaceUuid
     * @param string $locationRoomUuid
     * @return array
     */
    public function getLocationViewRacks(string $locationPlaceUuid, string $locationRoomUuid): array
    {
        return $this->locationViewTable->getLocationRacks($locationPlaceUuid, $locationRoomUuid);
    }

    /**
     * @param string $locationPlaceUuid
     * @return array
     */
    public function getLocationViewRooms(string $locationPlaceUuid): array
    {
        return $this->locationViewTable->getLocationRooms($locationPlaceUuid);
    }

    /**
     * @return array
     */
    public function getLocationPlaces(): array
    {
        return $this->locationPlaceTable->getLocationPlaces();
    }

    /**
     * @return array
     */
    public function getLocationPlacesUuidAssoc(): array
    {
        return $this->locationPlaceTable->getLocationPlacesUuidAssoc();
    }

    /**
     * @param string $locationPlaceUuid
     * @return array
     */
    public function getLocationRoomsForPlaceUuidAssoc(string $locationPlaceUuid): array
    {
        if (!$this->existLocationPlace($locationPlaceUuid)) {
            return [];
        }
        return $this->locationRoomTable->getLocationRoomsForPlaceUuidAssoc($locationPlaceUuid);
    }

    /**
     * @param string $locationRoomUuid
     * @return array
     */
    public function getLocationRacksForRoomUuidAssoc(string $locationRoomUuid): array
    {
        if (!$this->existLocationRoom($locationRoomUuid)) {
            return [];
        }
        return $this->locationRackTable->getLocationRacksForRoomUuidAssoc($locationRoomUuid);
    }

    /**
     * @param string $locationRackUuid
     * @return array
     */
    public function getLocationRowsForRackUuidAssoc(string $locationRackUuid): array
    {
        if (!$this->existLocationRack($locationRackUuid)) {
            return [];
        }
        return $this->locationRowTable->getLocationRowsForRackUuidAssoc($locationRackUuid);
    }

    /**
     * @param string $locationRowUuid
     * @return array
     */
    public function getLocationCasesForRowUuidAssoc(string $locationRowUuid): array
    {
        if (!$this->existLocationRow($locationRowUuid)) {
            return [];
        }
        return $this->locationCaseTable->getLocationCasesForRowUuidAssoc($locationRowUuid);
    }
}
