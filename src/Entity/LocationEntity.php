<?php

namespace Lerp\Location\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class LocationEntity extends AbstractEntity
{
    public array $mapping = [
        'location_case_uuid' => 'location_case_uuid',
        'location_case_id' => 'location_case_id',
        'location_case_name' => 'location_case_name',
        'location_case_label' => 'location_case_label',
        'location_row_uuid' => 'location_row_uuid',
        'location_row_id' => 'location_row_id',
        'location_row_name' => 'location_row_name',
        'location_row_label' => 'location_row_label',
        'location_rack_uuid' => 'location_rack_uuid',
        'location_rack_id' => 'location_rack_id',
        'location_rack_name' => 'location_rack_name',
        'location_rack_label' => 'location_rack_label',
        'location_room_uuid' => 'location_room_uuid',
        'location_room_id' => 'location_room_id',
        'location_room_name' => 'location_room_name',
        'location_room_label' => 'location_room_label',
        'location_place_uuid' => 'location_place_uuid',
        'location_place_id' => 'location_place_id',
        'location_place_name' => 'location_place_name',
        'location_place_label' => 'location_place_label',
        'location_label_compl' => 'location_label_compl',
        'location_label_place' => 'location_label_place',
        'location_label_room' => 'location_label_room',
        'location_label_arcr' => 'location_label_arcr',
    ];

    public function getLocationCaseUuid(): string
    {
        if (!isset($this->storage['location_case_uuid'])) {
            return '';
        }
        return $this->storage['location_case_uuid'];
    }

    public function setLocationCaseUuid(string $locationCaseUuid): void
    {
        $this->storage['location_case_uuid'] = $locationCaseUuid;
    }

    public function getLocationCaseId(): int
    {
        if (!isset($this->storage['location_case_id'])) {
            return 0;
        }
        return $this->storage['location_case_id'];
    }

    public function setLocationCaseId(int $locationCaseId): void
    {
        $this->storage['location_case_id'] = $locationCaseId;
    }

    public function getLocationCaseName(): string
    {
        if (!isset($this->storage['location_case_name'])) {
            return '';
        }
        return $this->storage['location_case_name'];
    }

    public function setLocationCaseName(string $locationCaseName): void
    {
        $this->storage['location_case_name'] = $locationCaseName;
    }

    public function getLocationCaseLabel(): string
    {
        if (!isset($this->storage['location_case_label'])) {
            return '';
        }
        return $this->storage['location_case_label'];
    }

    public function setLocationCaseLabel(string $locationCaseLabel): void
    {
        $this->storage['location_case_label'] = $locationCaseLabel;
    }

    public function getLocationRowUuid(): string
    {
        if (!isset($this->storage['location_row_uuid'])) {
            return '';
        }
        return $this->storage['location_row_uuid'];
    }

    public function setLocationRowUuid(string $locationRowUuid): void
    {
        $this->storage['location_row_uuid'] = $locationRowUuid;
    }

    public function getLocationRowId(): int
    {
        if (!isset($this->storage['location_row_id'])) {
            return 0;
        }
        return $this->storage['location_row_id'];
    }

    public function setLocationRowId(int $locationRowId): void
    {
        $this->storage['location_row_id'] = $locationRowId;
    }

    public function getLocationRowName(): string
    {
        if (!isset($this->storage['location_row_name'])) {
            return '';
        }
        return $this->storage['location_row_name'];
    }

    public function setLocationRowName(string $locationRowName): void
    {
        $this->storage['location_row_name'] = $locationRowName;
    }

    public function getLocationRowLabel(): string
    {
        if (!isset($this->storage['location_row_label'])) {
            return '';
        }
        return $this->storage['location_row_label'];
    }

    public function setLocationRowLabel(string $locationRowLabel): void
    {
        $this->storage['location_row_label'] = $locationRowLabel;
    }

    public function getLocationRackUuid(): string
    {
        if (!isset($this->storage['location_rack_uuid'])) {
            return '';
        }
        return $this->storage['location_rack_uuid'];
    }

    public function setLocationRackUuid(string $locationRackUuid): void
    {
        $this->storage['location_rack_uuid'] = $locationRackUuid;
    }

    public function getLocationRackId(): int
    {
        if (!isset($this->storage['location_rack_id'])) {
            return 0;
        }
        return $this->storage['location_rack_id'];
    }

    public function setLocationRackId(int $locationRackId): void
    {
        $this->storage['location_rack_id'] = $locationRackId;
    }

    public function getLocationRackName(): string
    {
        if (!isset($this->storage['location_rack_name'])) {
            return '';
        }
        return $this->storage['location_rack_name'];
    }

    public function setLocationRackName(string $locationRackName): void
    {
        $this->storage['location_rack_name'] = $locationRackName;
    }

    public function getLocationRackLabel(): string
    {
        if (!isset($this->storage['location_rack_label'])) {
            return '';
        }
        return $this->storage['location_rack_label'];
    }

    public function setLocationRackLabel(string $locationRackLabel): void
    {
        $this->storage['location_rack_label'] = $locationRackLabel;
    }

    public function getLocationRoomUuid(): string
    {
        if (!isset($this->storage['location_room_uuid'])) {
            return '';
        }
        return $this->storage['location_room_uuid'];
    }

    public function setLocationRoomUuid(string $locationRoomUuid): void
    {
        $this->storage['location_room_uuid'] = $locationRoomUuid;
    }

    public function getLocationRoomId(): int
    {
        if (!isset($this->storage['location_room_id'])) {
            return 0;
        }
        return $this->storage['location_room_id'];
    }

    public function setLocationRoomId(int $locationRoomId): void
    {
        $this->storage['location_room_id'] = $locationRoomId;
    }

    public function getLocationRoomName(): string
    {
        if (!isset($this->storage['location_room_name'])) {
            return '';
        }
        return $this->storage['location_room_name'];
    }

    public function setLocationRoomName(string $locationRoomName): void
    {
        $this->storage['location_room_name'] = $locationRoomName;
    }

    public function getLocationRoomLabel(): string
    {
        if (!isset($this->storage['location_room_label'])) {
            return '';
        }
        return $this->storage['location_room_label'];
    }

    public function setLocationRoomLabel(string $locationRoomLabel): void
    {
        $this->storage['location_room_label'] = $locationRoomLabel;
    }

    public function getLocationPlaceUuid(): string
    {
        if (!isset($this->storage['location_place_uuid'])) {
            return '';
        }
        return $this->storage['location_place_uuid'];
    }

    public function setLocationPlaceUuid(string $locationPlaceUuid): void
    {
        $this->storage['location_place_uuid'] = $locationPlaceUuid;
    }

    public function getLocationPlaceId(): int
    {
        if (!isset($this->storage['location_place_id'])) {
            return 0;
        }
        return $this->storage['location_place_id'];
    }

    public function setLocationPlaceId(int $locationPlaceId): void
    {
        $this->storage['location_place_id'] = $locationPlaceId;
    }

    public function getLocationPlaceName(): string
    {
        if (!isset($this->storage['location_place_name'])) {
            return '';
        }
        return $this->storage['location_place_name'];
    }

    public function setLocationPlaceName(string $locationPlaceName): void
    {
        $this->storage['location_place_name'] = $locationPlaceName;
    }

    public function getLocationPlaceLabel(): string
    {
        if (!isset($this->storage['location_place_label'])) {
            return '';
        }
        return $this->storage['location_place_label'];
    }

    public function setLocationPlaceLabel(string $locationPlaceLabel): void
    {
        $this->storage['location_place_label'] = $locationPlaceLabel;
    }

    public function getLocationLabelCompl(): string
    {
        if (!isset($this->storage['location_label_compl'])) {
            return '';
        }
        return $this->storage['location_label_compl'];
    }

    public function setLocationLabelCompl(string $locationLabelCompl): void
    {
        $this->storage['location_label_compl'] = $locationLabelCompl;
    }

    public function getLocationLabelPlace(): string
    {
        if (!isset($this->storage['location_label_place'])) {
            return '';
        }
        return $this->storage['location_label_place'];
    }

    public function setLocationLabelPlace(string $locationLabelPlace): void
    {
        $this->storage['location_label_place'] = $locationLabelPlace;
    }

    public function getLocationLabelRoom(): string
    {
        if (!isset($this->storage['location_label_room'])) {
            return '';
        }
        return $this->storage['location_label_room'];
    }

    public function setLocationLabelRoom(string $locationLabelRoom): void
    {
        $this->storage['location_label_room'] = $locationLabelRoom;
    }

    public function getLocationLabelArcr(): string
    {
        if (!isset($this->storage['location_label_arcr'])) {
            return '';
        }
        return $this->storage['location_label_arcr'];
    }

    public function setLocationLabelArcr(string $locationLabelArcr): void
    {
        $this->storage['location_label_arcr'] = $locationLabelArcr;
    }
}
