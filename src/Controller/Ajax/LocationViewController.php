<?php

namespace Lerp\Location\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Lerp\Location\Service\LocationService;
use Laminas\Http\Response;

class LocationViewController extends AbstractUserController
{
    protected LocationService $locationService;

    public function setLocationService(LocationService $locationService): void
    {
        $this->locationService = $locationService;
    }

    /**
     * @return JsonModel
     */
    public function locationViewPlacesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $places = $this->locationService->getLocationPlaces();
        $jsonModel->setArr($places);
        $jsonModel->setCount(count($places));
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function locationViewRoomsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $locationPlaceUuid = filter_var($this->params()->fromQuery('location_place_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $rooms = $this->locationService->getLocationViewRooms($locationPlaceUuid);
        $jsonModel->setArr($rooms);
        $jsonModel->setCount(count($rooms));
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function locationViewRacksAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $locationPlaceUuid = filter_var($this->params()->fromQuery('location_place_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $locationRoomUuid = filter_var($this->params()->fromQuery('location_room_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $racks = $this->locationService->getLocationViewRacks($locationPlaceUuid, $locationRoomUuid);
        $jsonModel->setArr($racks);
        $jsonModel->setCount(count($racks));
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function locationViewRowsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $locationPlaceUuid = filter_var($this->params()->fromQuery('location_place_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $locationRoomUuid = filter_var($this->params()->fromQuery('location_room_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $locationRackUuid = filter_var($this->params()->fromQuery('location_rack_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $rows = $this->locationService->getLocationViewRows($locationPlaceUuid, $locationRoomUuid, $locationRackUuid);
        $jsonModel->setArr($rows);
        $jsonModel->setCount(count($rows));
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function locationViewCasesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $locationPlaceUuid = filter_var($this->params()->fromQuery('location_place_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $locationRoomUuid = filter_var($this->params()->fromQuery('location_room_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $locationRackUuid = filter_var($this->params()->fromQuery('location_rack_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $locationRowUuid = filter_var($this->params()->fromQuery('location_row_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $cases = $this->locationService->getLocationViewCases($locationPlaceUuid, $locationRoomUuid, $locationRackUuid, $locationRowUuid);
        $jsonModel->setArr($cases);
        $jsonModel->setCount(count($cases));
        return $jsonModel;
    }
}
