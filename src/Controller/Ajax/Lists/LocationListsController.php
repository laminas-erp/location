<?php

namespace Lerp\Location\Controller\Ajax\Lists;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Location\Service\LocationService;

class LocationListsController extends AbstractUserController
{
    protected LocationService $locationService;

    public function setLocationService(LocationService $locationService): void
    {
        $this->locationService = $locationService;
    }

    /**
     * @return JsonModel
     */
    public function locationPlaceAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->locationService->getLocationPlacesUuidAssoc());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * Param: location_place_uuid
     * @return JsonModel
     */
    public function locationRoomAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $locationPlaceUuid = $this->params('place', '');
        if(!(new Uuid())->isValid($locationPlaceUuid) || !$this->locationService->existLocationPlace($locationPlaceUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->locationService->getLocationRoomsForPlaceUuidAssoc($locationPlaceUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * Param: location_room_uuid
     * @return JsonModel
     */
    public function locationRackAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $locationRoomUuid = $this->params('room', '');
        if(!(new Uuid())->isValid($locationRoomUuid) || !$this->locationService->existLocationRoom($locationRoomUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->locationService->getLocationRacksForRoomUuidAssoc($locationRoomUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * Param: location_rack_uuid
     * @return JsonModel
     */
    public function locationRowAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $locationRackUuid = $this->params('rack', '');
        if(!$this->locationService->existLocationRack($locationRackUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->locationService->getLocationRowsForRackUuidAssoc($locationRackUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * Param: location_row_uuid
     * @return JsonModel
     */
    public function locationCaseAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $locationRowUuid = $this->params('row', '');
        if(!$this->locationService->existLocationRow($locationRowUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->locationService->getLocationCasesForRowUuidAssoc($locationRowUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
