<?php

namespace Lerp\Location\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Lerp\Location\Service\LocationService;
use Laminas\Http\Response;

class LocationPlaceRestController extends AbstractUserRestController
{
    protected LocationService $locationService;

    public function setLocationService(LocationService $locationService): void
    {
        $this->locationService = $locationService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $locationPlaceName = filter_var($this->params()->fromPost('location_place_name', ''), FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if(empty($locationPlaceName)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if(!empty($locationPlaceUuid = $this->locationService->insertLocationPlace($locationPlaceName))) {
            $jsonModel->setSuccess(1);
            $jsonModel->setVariable('location_place_uuid', $locationPlaceUuid);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
