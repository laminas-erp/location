<?php

namespace Lerp\Location\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Lerp\Location\Service\LocationService;
use Laminas\Http\Response;

class LocationRestController extends AbstractUserRestController
{
    protected LocationService $locationService;

    public function setLocationService(LocationService $locationService): void
    {
        $this->locationService = $locationService;
    }

    /**
     * Create a complete location in one request.
     *
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $type = filter_var($data['type'], FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $placeId = filter_var($data['place_id'], FILTER_SANITIZE_NUMBER_INT);
        $placeName = filter_var($data['place_name'], FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $roomId = filter_var($data['room_id'], FILTER_SANITIZE_NUMBER_INT);
        $roomName = filter_var($data['room_name'], FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $rackId = filter_var($data['rack_id'], FILTER_SANITIZE_NUMBER_INT);
        $rowId = filter_var($data['row_id'], FILTER_SANITIZE_NUMBER_INT);
        $rowQntty = filter_var($data['row_quantity'], FILTER_SANITIZE_NUMBER_INT);
        $caseId = filter_var($data['case_id'], FILTER_SANITIZE_NUMBER_INT);
        $columnQntty = filter_var($data['column_quantity'], FILTER_SANITIZE_NUMBER_INT);
        if (
            (empty($type) && empty($placeId))
            || (empty($placeId) && $placeName)
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
