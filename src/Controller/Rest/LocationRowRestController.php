<?php

namespace Lerp\Location\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;
use Lerp\Location\Service\LocationService;
use Laminas\Http\Response;

class LocationRowRestController extends AbstractUserRestController
{
    protected LocationService $locationService;

    public function setLocationService(LocationService $locationService): void
    {
        $this->locationService = $locationService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $rackUuid =   filter_var($data['rack_uuid'], FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $rowName =  filter_var($data['row_name'], FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (!(new Uuid())->isValid($rackUuid) || !$this->locationService->existLocationRack($rackUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($rowId = $this->locationService->insertLocationRow($rackUuid, $rowName))) {
            $jsonModel->setSuccess(1);
            $jsonModel->setVariable('row_id', $rowId);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $locationRackUuid = filter_var($this->params()->fromQuery('location_rack_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if(!(new Uuid())->isValid($locationRackUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->locationService->getLocationRows($locationRackUuid));
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
