<?php

namespace Lerp\Location\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;
use Lerp\Location\Service\LocationService;
use Laminas\Http\Response;

class LocationCaseRestController extends AbstractUserRestController
{
    protected LocationService $locationService;

    public function setLocationService(LocationService $locationService): void
    {
        $this->locationService = $locationService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $rowUuid = filter_var($data['row_uuid'], FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $caseName = filter_var($data['case_name'], FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (!$this->locationService->existLocationRow($rowUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($caseUuid = $this->locationService->insertLocationCase($rowUuid, $caseName))) {
            $jsonModel->setSuccess(1);
            $jsonModel->setVariable('case_uuid', $caseUuid);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * Update the location case name.
     * @param string $id location_case_uuid
     * @param array $data Array with key 'case_name' & 'case_label' ...both can not be empty.
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $uuid = new Uuid();
        if (
            !$uuid->isValid($id)
            || empty($caseName = filter_var($data['case_name'], FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]))
            || empty($caseLabel = filter_var($data['case_label'], FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->locationService->updateLocationCaseName($id, $caseName, $caseLabel)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET Location cases for row uuid.
     *
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $locationRowUuid = filter_var($this->params()->fromQuery('location_row_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $jsonModel->setArr($this->locationService->getLocationCases($locationRowUuid));
        return $jsonModel;
    }

    /**
     * @param mixed $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
