<?php

namespace Lerp\Location;

use Lerp\Location\Controller\Ajax\LocationViewController;
use Lerp\Location\Controller\Rest\LocationCaseRestController;
use Lerp\Location\Controller\Rest\LocationRestController;
use Lerp\Location\Controller\Rest\LocationPlaceRestController;
use Lerp\Location\Controller\Rest\LocationRackRestController;
use Lerp\Location\Controller\Rest\LocationRoomRestController;
use Lerp\Location\Controller\Rest\LocationRowRestController;
use Lerp\Location\Factory\Controller\Ajax\LocationViewControllerFactory;
use Lerp\Location\Factory\Controller\Rest\LocationCaseRestControllerFactory;
use Lerp\Location\Factory\Controller\Rest\LocationRestControllerFactory;
use Lerp\Location\Factory\Controller\Rest\LocationPlaceRestControllerFactory;
use Lerp\Location\Factory\Controller\Rest\LocationRackRestControllerFactory;
use Lerp\Location\Factory\Controller\Rest\LocationRoomRestControllerFactory;
use Lerp\Location\Factory\Controller\Rest\LocationRowRestControllerFactory;
use Lerp\Location\Factory\Service\LocationServiceFactory;
use Lerp\Location\Factory\Table\LocationCaseTableFactory;
use Lerp\Location\Factory\Table\LocationPlaceTableFactory;
use Lerp\Location\Factory\Table\LocationRackTableFactory;
use Lerp\Location\Factory\Table\LocationRoomTableFactory;
use Lerp\Location\Factory\Table\LocationRowTableFactory;
use Lerp\Location\Factory\Table\LocationViewTableFactory;
use Lerp\Location\Service\LocationService;
use Lerp\Location\Table\LocationCaseTable;
use Lerp\Location\Table\LocationPlaceTable;
use Lerp\Location\Table\LocationRackTable;
use Lerp\Location\Table\LocationRoomTable;
use Lerp\Location\Table\LocationRowTable;
use Lerp\Location\Table\LocationViewTable;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'lerp_location_rest_location' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/lerp-location-rest-location[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults' => [
                        'controller' => LocationRestController::class
                    ],
                ],
            ],
            'lerp_location_rest_locationplace' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/lerp-location-rest-place[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults' => [
                        'controller' => LocationPlaceRestController::class
                    ],
                ],
            ],
            'lerp_location_rest_locationroom' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/lerp-location-rest-room[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults' => [
                        'controller' => LocationRoomRestController::class
                    ],
                ],
            ],
            'lerp_location_rest_locationrack' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/lerp-location-rest-rack[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults' => [
                        'controller' => LocationRackRestController::class
                    ],
                ],
            ],
            'lerp_location_rest_locationrow' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/lerp-location-rest-row[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults' => [
                        'controller' => LocationRowRestController::class
                    ],
                ],
            ],
            'lerp_location_rest_locationcase' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/lerp-location-rest-case[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]*',
                    ],
                    'defaults' => [
                        'controller' => LocationCaseRestController::class
                    ],
                ],
            ],
            /*
             * View
             */
            'lerp_location_view_locationplaces' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/lerp-location-view-places',
                    'defaults' => [
                        'controller' => LocationViewController::class,
                        'action' => 'locationViewPlaces'
                    ],
                ],
            ],
            'lerp_location_view_locationrooms' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/lerp-location-view-rooms',
                    'defaults' => [
                        'controller' => LocationViewController::class,
                        'action' => 'locationViewRooms'
                    ],
                ],
            ],
            'lerp_location_view_locationracks' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/lerp-location-view-racks',
                    'defaults' => [
                        'controller' => LocationViewController::class,
                        'action' => 'locationViewRacks'
                    ],
                ],
            ],
            'lerp_location_view_locationrows' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/lerp-location-view-rows',
                    'defaults' => [
                        'controller' => LocationViewController::class,
                        'action' => 'locationViewRows'
                    ],
                ],
            ],
            'lerp_location_view_locationcases' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/lerp-location-view-cases',
                    'defaults' => [
                        'controller' => LocationViewController::class,
                        'action' => 'locationViewCases'
                    ],
                ],
            ],
            /*
             * AJAX
             */
            'lerp_location_ajax_location_createdefaults' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/lerp-location-create-defaults',
                    'defaults' => [
                        'controller' => Controller\Ajax\LocationAjaxController::class,
                        'action' => 'createDefaults'
                    ],
                ],
            ],
            /*
             * AJAX - lists
             */
            'lerp_location_ajax_lists_locationplaceassoc' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/lerp-location-lists-locationplace-assoc',
                    'defaults' => [
                        'controller' => Controller\Ajax\Lists\LocationListsController::class,
                        'action' => 'locationPlaceAssoc'
                    ],
                ],
            ],
            'lerp_location_ajax_lists_locationroomassoc' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/lerp-location-lists-locationroom-assoc/:place',
                    'constraints' => [
                        'place' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\Ajax\Lists\LocationListsController::class,
                        'action' => 'locationRoomAssoc'
                    ],
                ],
            ],
            'lerp_location_ajax_lists_locationrackassoc' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/lerp-location-lists-locationrack-assoc/:room',
                    'constraints' => [
                        'room' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\Ajax\Lists\LocationListsController::class,
                        'action' => 'locationRackAssoc'
                    ],
                ],
            ],
            'lerp_location_ajax_lists_locationrowassoc' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/lerp-location-lists-locationrow-assoc/:rack',
                    'constraints' => [
                        'rack' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\Ajax\Lists\LocationListsController::class,
                        'action' => 'locationRowAssoc'
                    ],
                ],
            ],
            'lerp_location_ajax_lists_locationcaseassoc' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/lerp-location-lists-locationcase-assoc/:row',
                    'constraints' => [
                        'row' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\Ajax\Lists\LocationListsController::class,
                        'action' => 'locationCaseAssoc'
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            LocationRestController::class => LocationRestControllerFactory::class,
            LocationPlaceRestController::class => LocationPlaceRestControllerFactory::class,
            LocationRoomRestController::class => LocationRoomRestControllerFactory::class,
            LocationRackRestController::class => LocationRackRestControllerFactory::class,
            LocationRowRestController::class => LocationRowRestControllerFactory::class,
            LocationCaseRestController::class => LocationCaseRestControllerFactory::class,
            LocationViewController::class => LocationViewControllerFactory::class,
            Controller\Ajax\Lists\LocationListsController::class => Factory\Controller\Ajax\Lists\LocationListsControllerFactory::class,
            Controller\Ajax\LocationAjaxController::class => Factory\Controller\Ajax\LocationAjaxControllerFactory::class
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [
            LocationService::class => LocationServiceFactory::class,
            // table
            LocationPlaceTable::class => LocationPlaceTableFactory::class,
            LocationRoomTable::class => LocationRoomTableFactory::class,
            LocationRackTable::class => LocationRackTableFactory::class,
            LocationRowTable::class => LocationRowTableFactory::class,
            LocationCaseTable::class => LocationCaseTableFactory::class,
            LocationViewTable::class => LocationViewTableFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helper_config' => [
        'factories' => [],
        'invokables' => [],
        'aliases' => [],
    ],
    'view_manager' => [
        'template_map' => [],
        'template_path_stack' => [],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'lerp_location' => [
        'label_pres' => [
            'place' => 'P',
            'room' => 'Rm',
            'rack' => 'Rg',
            'row' => 'Rh',
            'case' => 'F',
        ]
    ],
];
